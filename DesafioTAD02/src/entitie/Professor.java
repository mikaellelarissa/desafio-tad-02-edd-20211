package entitie;

import service.ProfessorSet;

import java.util.List;

public class Professor implements ProfessorSet {
	
	private String nome;
	
	public Professor() {
		
	}

	public Professor(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean contains(List<Professor> professor, Professor professor1) {
		boolean consultar;
		consultar = professor.contains(professor1);
		return consultar;
	}

	@Override
	public boolean add(List<Professor> professor, Professor professor1) {
		boolean adicionar;
		adicionar = professor.add(professor1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Professor> professor, Professor professor1) {
		boolean remover;
		remover = professor.remove(professor1);
		return remover;
	}

	@Override
	public int size(List<Professor> professor) {
		return professor.size();
	}

	@Override
	public void clear(List<Professor> professor) {
		professor.clear();
	}
	
}
