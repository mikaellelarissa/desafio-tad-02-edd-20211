package service;

import entitie.Professor;

import java.util.List;

public interface ProfessorSet {
	
	public boolean contains(List<Professor> professor, Professor professor1);
	
	public boolean add(List<Professor> professor, Professor professor1);
	
	public boolean remove(List<Professor> professor, Professor professor1);
	
	public int size(List<Professor> professor);
	
	public void clear(List<Professor> professor);
}
