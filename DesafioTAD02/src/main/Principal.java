package main;

import entitie.Professor;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	static List<Professor> professor = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String nome, r;
		Professor p = new Professor();

		char escolha = 's';
		while (escolha == 's') {
			System.out.println("Informe o nome do professor que deseja cadastrar: ");
			nome = sc.nextLine();

			p = new Professor();
			p.add(professor, p);
			System.out.println("Deseja cadastrar um nome professor? s - sim   n - n�o");
			escolha = sc.nextLine().charAt(0);
		}

		System.out.println("Deseja consultar o professor? s - sim   n - n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			String teste = " ";
			if (p.contains(professor, p)) {
				teste = "existe.";
			} else {
				teste = "n�o existe.";
			}
			System.out.println("O professor " + teste);
		}

		System.out.println("Deseja remover um professor? s - sim   n - n�o");
		r = sc.nextLine();
		if(r.equals("s")) {
			System.out.println(p.remove(professor, p));
		} 
		
		System.out.println("A quantidade de professores � " + p.size(professor));
		
		System.out.println("Deseja limpar a lista de professores? s - sim   n - n�o");
		r = sc.nextLine();
		if(r.equals("s")) {
			p.clear(professor);
			System.out.println("A quantidade de professores agora � " + p.size(professor));
		} else {
			System.out.println("A quantidade de professores � " + p.size(professor));
		}
	}
}